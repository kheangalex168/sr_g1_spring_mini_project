import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useLocation,
  useHistory,
} from "react-router-dom";

export default function Detail() {
  let param = useParams();
  return (
    <div>
      <h1>Detail: {param.id}</h1>
    </div>
  );
}
