import React, { Component } from "react";
import { Redirect } from "react-router";
import Auth from "./Auth";

export default function ProtectedRoute({
  sign,
  component: Component,
  path,
  OnSign,
}) {
  if (sign) {
    return <Component path={path} OnSign={OnSign} />;
  } else {
    return <Redirect to="/auth" />;
  }
}
