import React from "react";
import MyCard from "./Card";
import Detail from "./Detail";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useLocation,
  useHistory,
} from "react-router-dom";

export default function Home(props) {
  return (
    <Router>
      <Switch>
        <Route path={"/detail/:id"} component={Detail} />
      </Switch>
      <div id="card">
        {props.item.map((item, i) => (
          <MyCard item={item} i={i} clear={props.clear} />
        ))}
      </div>
    </Router>
  );
}
