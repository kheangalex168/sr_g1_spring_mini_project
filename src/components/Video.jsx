import React from "react";
import { Button } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useLocation,
} from "react-router-dom";

export default function Video() {
  let { path, url } = useRouteMatch();
  return (
    <Router>
      <div>
        <h1>Video</h1>
        <Link as={Link} to={`${url}/movie`}>
          <Button>Movie</Button>
        </Link>
        <Link as={Link} to={`${url}/animation`}>
          <Button>Animation</Button>
        </Link>

        <Switch>
          <Route path={`${path}/movie`} component={Movie} />
          <Route path={`${path}/animation`} component={Animation} />
        </Switch>
      </div>
    </Router>
  );
}

function Animation() {
  let { path, url } = useRouteMatch();

  return (
    <div>
      <h2>Animation Category</h2>
      <ul>
        <li>
          <Link to={`${url}?type=Action`}>
            <Button>Action</Button>{" "}
          </Link>
        </li>
        <li>
          <Link to={`${url}?type=Romance`}>
            <Button>Romance</Button>{" "}
          </Link>
        </li>
        <li>
          <Link to={`${url}?type=Comedy`}>
            <Button>Comedy</Button>{" "}
          </Link>
        </li>
      </ul>
      <Switch>
        <Route path={path}>
          <h3>
            Please Choose Category: <Category />
          </h3>
        </Route>
      </Switch>
    </div>
  );
}

function Movie() {
  let { path, url } = useRouteMatch();

  return (
    <div>
      <h2>Movie Category</h2>
      <ul>
        <li>
          <Link to={`${url}?type=Adventure`}>
            <Button>Adventue</Button>{" "}
          </Link>
        </li>
        <li>
          <Link to={`${url}?type=Crime`}>
            <Button>Crime</Button>{" "}
          </Link>
        </li>
        <li>
          <Link to={`${url}?type=Action`}>
            <Button>Action</Button>{" "}
          </Link>
        </li>
        <li>
          <Link to={`${url}?type=Romance`}>
            <Button>Romance</Button>{" "}
          </Link>
        </li>
        <li>
          <Link to={`${url}?type=Comedy`}>
            <Button>Comedy</Button>{" "}
          </Link>
        </li>
      </ul>
      <Switch>
        <Route path={path}>
          <h3>
            Please Choose Category: <Category />
          </h3>
        </Route>
      </Switch>
    </div>
  );
}

function Category() {
  const location = useLocation();
  console.log(location);
  const query = new URLSearchParams(location.search);
  const type = query.get("type");
  return (
    <div>
      <h3 style={{ color: "red" }}>{type}</h3>
    </div>
  );
}
