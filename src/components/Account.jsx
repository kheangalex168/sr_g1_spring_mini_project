import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
} from "react-router-dom";

export default function Account() {
  let { path, url } = useRouteMatch();
  return (
    <Router>
      <div>
        <h2>Accounts</h2>

        <ul>
          <li>
            <Link to={`${path}/netflix`}>Netflix</Link>
          </li>
          <li>
            <Link to={`${path}/zillow-groups`}>Zillow Group</Link>
          </li>
          <li>
            <Link to={`${path}/yahoo`}>Yahoo</Link>
          </li>
          <li>
            <Link to={`${path}/modus-create`}>Modus Create</Link>
          </li>
        </ul>

        <Switch>
          <Route path={`${path}/:id`} children={<Child />} />
        </Switch>
      </div>
    </Router>
  );
}

function Child() {
  let { id } = useParams();

  return (
    <div>
      <h3>ID: {id}</h3>
    </div>
  );
}
