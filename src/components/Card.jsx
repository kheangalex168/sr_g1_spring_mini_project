import React from "react";
import { Card, Button } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useLocation,
} from "react-router-dom";

export default function MyCard(props) {
  return (
    <div>
      <Card style={{ width: "18rem" }} key={props.i}>
        <Card.Img variant="top" src={props.item.img} />
        <Card.Body>
          <Card.Title>{props.item.title}</Card.Title>
          <Card.Text>{props.item.content}</Card.Text>
          <Link as={Link} to={`/detail/${props.i + 1}`}>
            <Button onClick={() => props.clear(props.item.id)}>Read</Button>
          </Link>
        </Card.Body>
      </Card>
    </div>
  );
}
