import React from "react";
import { Form, Button } from "react-bootstrap";

export default function Welcome(props) {
  return (
    <div>
      <h1>Welcome</h1>
      <Button onClick={props.OnSign} type="submit">
        Logout
      </Button>
    </div>
  );
}
